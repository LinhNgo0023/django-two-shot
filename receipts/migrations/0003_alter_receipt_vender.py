# Generated by Django 4.2.3 on 2023-07-19 19:30

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0002_alter_receipt_vender"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="vender",
            field=models.CharField(max_length=200),
        ),
    ]
